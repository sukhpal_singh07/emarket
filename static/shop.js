function get_products(q='all'){
    $.ajax({
        url:"/api/products",
        type:'get',
        data:q,
        success:function(data){
            result = '';
            for(i in data){
                result+='<div class="col-lg-4 col-md-6 col-sm-12 my-5" id="a'+data[i].id+'" onmouseover=image_hover('+data[i].id+',"'+data[i].images[1].product_image+'") onmouseout=image_hide('+data[i].id+',"'+data[i].images[0].product_image+'")>';
                result+='<img class="product_img'+data[i].id+'" id="product"  src="/media/'+data[i].images[0].product_image+'" >';
                // result+='<button class="btn button px-3 py-2 " value="Add To Cart" onclick="add('+data[i].id+') type="submit" id="cart'+data[i].id+'" >Add To Cart</button>    '            
                result+='<i class="far fa-heart hrticn" id="heart'+data[i].id+'" data-toggle="tooltip" data-placement="top" onclick=add_to_favourite("'+data[i].id+'") title="Add to wishlist"></i>'
                result+='<p class="font-weight-bold mt-2  titlename"><a href="/product_detail/?id='+data[i].id+'" class="text-dark">'+data[i].name+'</a></p>';
                result+='<p class="text-secondary mt-4 mb-1">'+data[i].brand+'</p>';
                price = data[i].price;
                discount = data[i].discount;
                discounted_price = price-discount;
                dis_per = Math.round((discount/price)*100)
                if(discounted_price < price){
                    result+='<del class="text-secondary text-cancel">$'+price+'</del><span class="ml-2">$'+discounted_price+'</span>'
                    result+=' <div class="badge py-2 ml-1" style="background-color: rgb(255, 0, 0); position: absolute;top: 10px;left: 20px;color: white;text-align: center;">'+dis_per+'% off</div>'
                }
                else{
                    result+='<span class="">$'+price+'</span>'
                }
                result+='</div>';
            }
            $("#product_found").html(data.length);
            $("#products").html(result);
            fav_list();
        }
    });
    }
    get_products();

    function image_hover(id,image){
        $(".product_img"+id).attr("src","/media/"+image)
    }
    function image_hide(id,image){
        $(".product_img"+id).attr("src","/media/"+image)
    }
    function add_to_favourite(id){
        $.ajax({
            url:'/add_to_favourite',
            type:'get',
            data:{id:id},
            success:function(data){
                if(data.status==0){
                    alert(data.message);
                }else{
                    if(data.status==1){
                        $("#heart"+id).removeClass("far fa-heart").addClass("fas fa-heart")
                    }else if(data.status==2){
                        $("#heart"+id).removeClass("fas fa-heart").addClass("far fa-heart")
                    }
                    else{
                        alert("Somethng went wrong!");
                    }
                }
            }
        })
    }

    function fav_list(){
        $.ajax({
            url:'/all_favourites',
            type:'get',
            success:function(data){
                if(data.favourites.length>1){
                    for(i in data.favourites){
                        $("#heart"+data.favourites[i]).removeClass("far fa-heart").addClass("fas fa-heart")
                    }
                }
            }
        })
    }
    fav_list();
