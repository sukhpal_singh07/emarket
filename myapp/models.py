from django.db import models
from django.db.models import fields
from django.contrib.auth.models import User
from django.db.models.expressions import F

# Create your models here.

class Contact_Us(models.Model):
    name = models.CharField(max_length=250)
    email = models.EmailField(unique=True)
    subject = models.CharField(max_length=500)
    message = models.TextField()
    is_approved = models.BooleanField(default = False)
    received_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Contact Table"

class Brand(models.Model):
    name = models.CharField(max_length=250)
    image = models.ImageField(upload_to="brands/%Y/%m/%d")
    added_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Brand Table"

class Customer_Category(models.Model):
    name = models.CharField(max_length=250)
    added_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Product_Category(models.Model):
    customer_category =  models.ForeignKey(Customer_Category, on_delete = models.CASCADE)
    category_name = models.CharField(max_length=250)
    category_image = models.ImageField(upload_to="category/%Y/%m/%d")
    added_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.category_name

class Product(models.Model):
    colors = (
        ("Red","Red"),
        ("Green","Green"),
        ("Blue","Blue"),
        ("Black","Black"),
        ("White","White"),
        ("Pink","Pink"),
        ("Orange","Orange"),
        ("Green","Green"),
        ("Yellow","Yellow"),
    )
    sizes = (
        ("S","S"),
        ("M","M"),
        ("L","L"),
        ("XL","XL"),
    )
    category = models.ForeignKey(Product_Category, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    price = models.FloatField()
    discount = models.FloatField(default=0)
    color = models.CharField(max_length=250, choices=colors, blank=True)
    size = models.CharField(max_length=250, choices=sizes, blank=True)
    description = models.TextField(blank=True)
    is_available = models.BooleanField(default=True)
    added_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Product Table"

class Product_Images(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    product_image = models.ImageField(upload_to="products/%Y/%m/%d")

    def __str__(self):
        return self.product.name

    class Meta:
        verbose_name_plural = "Product Images"


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_pic = models.ImageField(upload_to = "profile/%Y/%m/%d", null=True)
    phone_number = models.IntegerField(blank=True)
    email_address= models.CharField(blank=True,null=True,max_length=250)
    registered_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)


    def __str__(self):
        return self.user.first_name + " " +self.user.last_name

    class Meta:
        verbose_name_plural = "User Profile Table"

class Add_To_Favourite(models.Model):
    customer = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.customer.user.first_name

class cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    image = models.ImageField(default=False)
    total_price=models.FloatField(default=False)
    quantity = models.IntegerField()
    status = models.BooleanField(default=False)
    added_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.user.username

class checkout(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    country = models.CharField(max_length=250)
    state = models.CharField(max_length=250)
    city =  models.CharField(max_length=250)
    address = models.CharField(max_length=250)
    postcode = models.IntegerField(blank=True)
    phone_number = models.IntegerField(blank=True)
    email_address = models.EmailField(max_length=250)
    is_approved = models.BooleanField(default = False)
    received_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.first_name

    class Meta:
        verbose_name_plural = "Checkout Table"

class Order(models.Model):
    cust_info= models.ForeignKey(checkout, on_delete=models.CASCADE,null=True)
    cust_id = models.ForeignKey(User, on_delete=models.CASCADE)
    cart_ids = models.CharField(max_length=250)
    product_ids = models.CharField(max_length=250)
    invoice_id = models.CharField(max_length=250)
    status=models.BooleanField(default=False)
    processed_on= models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.cust_id.first_name